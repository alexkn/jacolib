import os
import re

from . import utils

# ==================================================================================================

langdicts = None
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"

# ==================================================================================================


def get_langdicts() -> dict:
    """Load the langdicts, or just return them if they are already loaded"""
    global langdicts

    if langdicts is None:
        path = file_path + "data/langdicts.json"
        langdicts = utils.load_json_file(path)

    return langdicts


# ==================================================================================================


def clean_text_for_stt(text: str, language: str) -> str:
    """Clean a text that it only consists of chars used in stt.
    Moved here because this may be useful for some skills too."""

    lds = get_langdicts()
    allowed_lang = lds["allowed_chars"][language]
    replacer_lang = lds["replacers"][language]

    text = clean_text(text, replacer_lang, allowed_lang)
    return text


# ==================================================================================================


def clean_text(text: str, replacers: dict, allowed: str) -> str:
    """Clean a text by replacing and removing characters,
    so that it only consists of the given allowed chars"""

    text = text.lower()
    for c in replacers.keys():
        text = text.replace(c, replacers[c])
    text = re.sub(r"[^" + allowed + " " + "]", "", text)

    return text
