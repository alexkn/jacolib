import json
import os
import threading
import traceback
from typing import Any, Callable, Optional

from cryptography import fernet
from paho.mqtt.client import Client, MQTTMessage

from . import utils

# ==================================================================================================

encryptors = {}
topic_keys = None
skill_path = ""


# ==================================================================================================


def set_skill_path(path: str) -> None:
    """Needed to use topic keys loading without extra parameter for skills too"""
    global skill_path
    skill_path = path


# ==================================================================================================


def try_con_callback(on_connect_callback: Optional[Callable] = None) -> Callable:
    """Surround callback with try block and print all occurring exceptions
    Else they would be silenced in the callback thread"""

    def try_on_connect(
        client: Client,
        userdata: Any,  # pylint: disable=unused-argument
        flags: dict,  # pylint: disable=unused-argument
        rc: int,
    ) -> None:
        try:
            print("Connected to mqtt broker with result code:", rc)
            if on_connect_callback is not None:
                on_connect_callback(client)
        except Exception:  # pylint: disable=broad-except
            print("===== ERROR =========================")
            traceback.print_exc()
            print("=====================================")

    return try_on_connect


# ==================================================================================================


def try_msg_callback(on_message_callback: Callable) -> Callable:
    """Surround callback with try block and print all occurring exceptions, else they would be
    silenced in the callback thread. Also run each callback in a extra thread, to allow multiple
    callbacks handled simultaneously."""

    def try_on_message(client: Client, userdata: Any, msg: MQTTMessage) -> None:
        try:
            on_message_callback(client, userdata, msg)
        except Exception:  # pylint: disable=broad-except
            print("===== ERROR =========================")
            traceback.print_exc()
            print("=====================================")

    def try_cb_threaded(client: Client, userdata: Any, msg: MQTTMessage) -> None:
        thread = threading.Thread(target=try_on_message, args=(client, userdata, msg))
        thread.start()

    return try_cb_threaded


# ==================================================================================================


def connect_mqtt_client(
    on_connect_callback: Optional[Callable] = None,
    on_message_callback: Optional[Callable] = None,
) -> Client:
    """Connects and returns mqtt client using users config values"""

    global_config = utils.load_global_config()
    mqtt_host, mqtt_port = global_config["mqtt_broker_address"].split(":")
    username = global_config["mqtt_username"]
    password = global_config["mqtt_password"]

    client = Client()
    toc_cb = try_con_callback(on_connect_callback)
    client.on_connect = toc_cb
    if on_message_callback is not None:
        tom_cb = try_msg_callback(on_message_callback)
        client.on_message = tom_cb

    client.username_pw_set(username, password)
    client.connect(mqtt_host, int(mqtt_port))

    return client


# ==================================================================================================


def load_topic_keys() -> dict:
    """Get module and skill topic keys if they exist"""

    t_keys = {}

    path = utils.get_repo_path() + "userdata/module_topic_keys.json"
    if os.path.exists(path):
        with open(path, "r", encoding="utf-8") as file:
            module_keys = json.load(file)
        t_keys.update(module_keys)

    path = utils.get_repo_path() + "userdata/skill_topic_keys.json"
    if os.path.exists(path):
        with open(path, "r", encoding="utf-8") as file:
            skill_keys = json.load(file)
        t_keys.update(skill_keys)

    # If this is called from a skill, the keys are in a different location
    if skill_path != "":
        path = skill_path + "skilldata/topic_keys.json"
        with open(path, "r", encoding="utf-8") as file:
            skill_keys = json.load(file)
        t_keys.update(skill_keys)

    return t_keys


# ==================================================================================================


def get_all_topics() -> list:
    global topic_keys

    if topic_keys is None:
        topic_keys = load_topic_keys()

    return list(topic_keys.keys())


# ==================================================================================================


def get_encryptor(topic: str) -> fernet.Fernet:
    """Return encryptor object for given topic, create new one if necessary"""
    global topic_keys

    if topic_keys is None:
        topic_keys = load_topic_keys()

    if topic not in encryptors:
        if topic in topic_keys:
            encryptors[topic] = fernet.Fernet(topic_keys[topic])
        else:
            raise LookupError("This topic does not have a topic key: {}".format(topic))

    # This will raise an error (which will be silenced),
    # but this is a better location to raise it than somewhere later in the caller function
    return encryptors[topic]


# ==================================================================================================


def encrypt_msg(msg: dict, topic: str) -> bytes:
    encryptor = get_encryptor(topic)
    encoded = json.dumps(msg).encode("utf-8")
    encrypted: bytes = encryptor.encrypt(encoded)
    return encrypted


# ==================================================================================================


def decrypt_msg(msg: bytes, topic: str) -> dict:
    encryptor = get_encryptor(topic)
    try:
        decrypted = encryptor.decrypt(msg)
        decoded: dict = json.loads(decrypted)
        return decoded
    except (TypeError, json.decoder.JSONDecodeError, fernet.InvalidToken):
        print("Received a corrupt message on topic:", topic)
        return {}
