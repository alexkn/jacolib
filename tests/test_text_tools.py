from jacolib import text_tools

# ==================================================================================================


def test_clean_text_for_stt() -> None:
    text = "Löwenzahn"
    language = "de"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "loewenzahn"

    text = "I'dlik3much$$"
    language = "en"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "i'dlikmuch"

    text = "Me pica el bagre"
    language = "es"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "me pica el bagre"

    text = "Liberté, Égalité, Fraternité"
    language = "fr"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "liberte egalite fraternite"
