from typing import Any, List

from jacolib import assistant

# ==================================================================================================


def test_extract_entities() -> None:
    # Test simple intent
    sol: List[Any] = []
    msg = {
        "intent": {"name": "check_riddle", "confidence": 1.0},
        "text": "the answer to the riddle is rainbow",
        "entities": [{"entity": "riddle_answers", "value": "rainbow"}],
        "timestamp": 123.45,
    }
    entities = assistant.extract_entities(msg, "riddle_answers")
    sol = ["rainbow"]
    assert sol == entities

    # Test multiple slots
    msg = {
        "intent": {"name": "add_numbers", "confidence": 0.97},
        "text": "what is two plus three",
        "entities": [
            {"entity": "number", "value": 2},
            {"entity": "number", "value": 3},
        ],
        "timestamp": 123.45,
    }
    entities = assistant.extract_entities(msg, "number")
    sol = [2, 3]
    assert sol == entities
