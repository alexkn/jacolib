import pytest

from jacolib import extra_tools

# ==================================================================================================


def test_intent_to_topic() -> None:
    it = extra_tools.intent_to_topic("skill_riddles-get_riddle")
    assert it == "Jaco/Intents/SkillRiddles/GetRiddle"

    it = extra_tools.intent_to_topic(None)
    assert it == "Jaco/Intents/IntentNotRecognized"


# ==================================================================================================


def test_topic_to_intent() -> None:
    it = extra_tools.topic_to_intent("Jaco/Intents/SkillRiddles/GetRiddle")
    assert it == "skill_riddles-get_riddle"

    it = extra_tools.topic_to_intent("Jaco/Intents/IntentNotRecognized")
    assert it == "intent_not_recognized"

    with pytest.raises(ValueError):
        extra_tools.topic_to_intent("Jaco/SayText")
