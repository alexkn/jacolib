import os

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "data/")


# ==================================================================================================


def test_get_all_topics() -> None:
    tops = comm_tools.get_all_topics()
    assert tops == ["Jaco/Intents/CheckRiddle", "Jaco/Intents/GetRiddle"]

    # Load topic keys for skills
    utils.set_repo_path("")
    comm_tools.set_skill_path(file_path + "data/skills/skills/testskill/")
    tops = comm_tools.get_all_topics()
    assert tops == ["Jaco/Intents/CheckRiddle", "Jaco/Intents/GetRiddle"]


# ==================================================================================================


def test_encrypt_decrypt() -> None:
    msg = {"solution": "rainbow"}
    enc_msg = comm_tools.encrypt_msg(msg, "Jaco/Intents/CheckRiddle")
    dec_msg = comm_tools.decrypt_msg(enc_msg, "Jaco/Intents/CheckRiddle")

    assert enc_msg != msg
    assert dec_msg == msg
